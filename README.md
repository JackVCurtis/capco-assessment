# CapcoAssessment

This project demonstrates a simple table component which fulfills the following requirements -

The component should display Sample Data in a table
User should be able to select how many rows are displayed in the table
Table should be paginated if not all rows are displayed on the screen based on the
user’s selection
Pagination options should be displayed in the table footer
Column names should be displayed in the table header
Entire table, table header and table footer should always be displayed on the screen
while scrolling
If number of rows exceeds the size of the table body, a vertical scrollbar should be
displayed within the table body – only table body shall scroll vertically, table header and
footer shall remain as is
If number of columns exceed the size of the table body, a horizontal scrollbar should be
displayed within the table body – only table body and table header shall scroll to reveal
the additional columns, table footer shall remain as is
Each row should contain a button which shall submit the row ID and row status to
/api/submit as a POST request – You are not expected to create the POST endpoint,
but you can mock one if you like

The table component accepts two inputs - a data set ([data]) and an array of column definitions ([columns]). The column definition consists of an id, which matches the id of the property to display from the data object, a title for the column, and it's width in pixels. Optionally, you can provide a custom component to the column which will be rendered by the table in place of displaying the value from the data object. This custom component should have a 'data' property, which will be set to the value of the complete data object for its corresponding row.