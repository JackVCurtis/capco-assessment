import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
  Input,
  ComponentFactoryResolver,
  ComponentFactory,
  ComponentRef
} from '@angular/core';

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.scss']
})
export class RendererComponent implements OnInit, OnDestroy {

  constructor(private resolver: ComponentFactoryResolver) { }

  @ViewChild('templateRef', { read: ViewContainerRef })
  templateRef: ViewContainerRef;

  @Input()
  component: any;

  @Input()
  data: any;

  componentRef: ComponentRef<any>;

  ngOnInit() {
      this.templateRef.clear();
      if (this.component) {
        const component = this.component;
        const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(component);
        this.componentRef = this.templateRef.createComponent(factory);
        this.componentRef.instance.data = this.data;
      }
  }

  ngOnDestroy() {
   this.componentRef.destroy();
  }

}
