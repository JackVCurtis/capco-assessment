import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { HttpClientModule } from '@angular/common/http';

import { RendererComponent } from './renderer.component';
import { SubmitComponent } from '../submit/submit.component';

describe('RendererComponent', () => {
  let component: RendererComponent;
  let fixture: ComponentFixture<RendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      declarations: [ RendererComponent, SubmitComponent ]
    });
    TestBed.overrideModule(BrowserDynamicTestingModule, {
        set: {
            entryComponents: [SubmitComponent]
        }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendererComponent);
    component = fixture.componentInstance;
    component.component = SubmitComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render component input', () => {
    expect(fixture.nativeElement.querySelector('div')).toBeTruthy();
  });
});
