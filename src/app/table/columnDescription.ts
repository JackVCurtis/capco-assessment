export default interface ColumnDescription {
  id: string;
  title: string;
  width: number;
  component?: any;
}
