import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import ColumnDescription from './columnDescription';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor() { }

  totalWidth: string;

  @Input()
  data: any[];

  @Input()
  columns: ColumnDescription[];

  @ViewChild('appTable')
  private table: ElementRef;

  page = 1;
  pageSize = 20;

  ngOnInit() {
    this.totalWidth = this.columns.reduce((acc, col) => {
      return acc + col.width;
    }, 0) + 'px';
  }

  getPageData() {
    const startIndex = (this.page * this.pageSize) - this.pageSize;
    const endIndex = (this.page * this.pageSize);
    return this.data ? this.data.slice(startIndex, endIndex) : [];
  }

  getPages() {
    if (this.data) {
      const totalPages = Math.ceil(this.data.length / this.pageSize);
      const pages = [];
      for (let i = 0; i < totalPages; i++) {
        pages[i] = i + 1;
      }
      return pages;
    }
  }

  setPageSize(value) {
    this.pageSize = parseInt(value, 10);
  }

  setPage(p) {
    this.page = p;
    this.table.nativeElement.scrollTop = 0;
  }

  pageBack() {
    if (this.page > 1) {
      this.page = this.page - 1;
      this.table.nativeElement.scrollTop = 0;
    }
  }

  pageForward() {
    if (this.data.length > this.page * this.pageSize) {
      this.page = this.page + 1;
      this.table.nativeElement.scrollTop = 0;
    }
  }

}
