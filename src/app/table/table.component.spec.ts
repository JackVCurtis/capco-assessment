import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { RendererComponent } from '../renderer/renderer.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent, RendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    component.data = [{name: 'foo'}, {name: 'bar'}];
    component.columns = [{id: 'name', title: 'Name', width: 100}];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show only the data for the selected page', () => {
    component.pageSize = 1;
    expect(component.getPageData().length).toBe(1);
  });

  it('should show data for the appropriate page', () => {
    component.pageSize = 1;
    component.page = 2;
    expect(component.getPageData()[0].name).toBe('bar');
  });
});
