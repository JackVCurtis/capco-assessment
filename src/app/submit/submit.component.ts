import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.scss'],
  providers: [HttpClient]
})
export class SubmitComponent implements OnInit {

  constructor(private http: HttpClient) {}

  data: any;

  ngOnInit() {
  }

  submit() {
      this.http.post('/api/submit', {id: this.data.id, status: this.data.status})
          .subscribe(() => {}, () => { console.log('Endpoint not found'); });
  }
}
