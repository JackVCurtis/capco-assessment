import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SubmitComponent } from './submit/submit.component';
import ColumnDescription from './table/columnDescription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [HttpClient]
})
export class AppComponent implements OnInit {
    constructor(http: HttpClient) {
      this.http = http;
    }

    data: any[];
    http: HttpClient;
    columns: ColumnDescription[] = [
      {id: 'name', title: 'Name', width: 200},
      {id: 'phone', title: 'Phone', width: 200},
      {id: 'email', title: 'Email', width: 200},
      {id: 'company', title: 'Company', width: 200},
      {id: 'date_entry', title: 'Entry Date', width: 200},
      {id: 'org_num', title: 'Org Number', width: 200},
      {id: 'address_1', title: 'Main Address', width: 200},
      {id: 'city', title: 'City', width: 200},
      {id: 'zip', title: 'Zip Code', width: 200},
      {id: 'geo', title: 'Location', width: 200},
      {id: 'pan', title: 'PAN', width: 200},
      {id: 'pin', title: 'PIN', width: 200},
      {id: 'id', title: 'ID', width: 200},
      {id: 'status', title: 'Status', width: 200},
      {id: 'fee', title: 'Fee', width: 200},
      {id: 'guid', title: 'GUID', width: 200},
      {id: 'date_exit', title: 'Exit Date', width: 200},
      {id: 'date_recent', title: 'Most Recent', width: 200},
      {id: 'url', title: 'URL', width: 200},
      {id: 'submit', title: 'Submit', width: 200, component: SubmitComponent}
    ];

    ngOnInit() {
      this.http.get('/assets/sample_data.json')
          .subscribe((data: [any]) => {
              this.data = data;
          });
    }
}
